package INF102.lab3.peakElement;

import java.util.List;

public class PeakRecursive implements IPeak {

    @Override
    public int peakElement(List<Integer> numbers) {
        var n = numbers.size();
        
        // Cannot find peak in an empty list
        if (n == 0) throw new IllegalArgumentException("Empty list");

        // If the list only has one item, that item is a peak.
        if (n == 1) return numbers.get(0);

        // The edges must be greater than (and not equal to) the value
        // next to them, to be considered a peak.
        if (numbers.get(0) > numbers.get(1)) return numbers.get(0);
        if (numbers.get(n-1) > numbers.get(n-2)) return numbers.get(n-1);

        // Find the peak inside the array.
        return peekElement(numbers, numbers.get(0), numbers.get(1), 1);
    }

    private int peekElement(List<Integer> numbers, int previous, int current, int index) {
        var next = numbers.get(index+1);

        // A peak is greater than or equal to both the numbers next to it,
        if (current >= previous && current >= next) return current;

        // A guard against index out of bounds.
        if (index < numbers.size() - 1) return peekElement(numbers, current, next, index+1);
        throw new IllegalArgumentException("No peak element");
    }

}
