package INF102.lab3.sumList;

import java.util.List;

public class SumRecursive implements ISum {

    @Override
    public long sum(List<Long> list) {
        // The sum of 0 numbers is 0
        if (list.size() == 0) return 0;

        // The sum of 1 number is that number
        if (list.size() == 1) return list.get(0);

        // The sum of n numbers is the n-th number + the sum of the other n-1 numbers.
        return popFirstNumber(list) + sum(list);
    }
    
    private long popFirstNumber(List<Long> list) {
        // Remove and return the last number in the array.
        return list.remove(list.size() - 1);
    }
}
