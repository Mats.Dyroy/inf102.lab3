package INF102.lab3.numberGuesser;


public class MyGeniusGuesser implements IGuesser {

	@Override
    public int findNumber(RandomNumber number) {
        return findNumber(number, number.getLowerbound(), number.getUpperbound());
    }

    private int findNumber(RandomNumber number, int min, int max) {
        // Guess the middle of the available guesses,
        // this will give the most information if the guess
        // is wrong.
        var guess = (max + min) / 2;

        // Perform the guess.
        var result = number.guess(guess);

        // If the guess was too high, 
        // the value must be greater than min and less than guess.
        if (result > 0) return findNumber(number, min, guess);

        // If the guess was too low, 
        // the value must be lower than max and greater than guess.
        if (result < 0) return findNumber(number, guess, max);
        return guess;
    }
}
